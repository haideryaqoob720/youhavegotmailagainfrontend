import Vue from "vue";
import './plugins/axios'
import App from "./App.vue";
import router from "./router";
import Toasted from 'vue-toasted';
import Multiselect from 'vue-multiselect'
import VueChatScroll from 'vue-chat-scroll'
import noUiSlider from "nouislider";
import "nouislider/distribute/nouislider.css";


Vue.prototype.$slider = noUiSlider;
Vue.use(VueChatScroll)

const ToastedOptions = {
  position: 'bottom-center',
  duration: 7000,
  theme: 'bubble'
}

Vue.component('multiselect', Multiselect)

Vue.use(Toasted, ToastedOptions);

Vue.config.productionTip = false;

import 'vue-multiselect/dist/vue-multiselect.min.css';

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
