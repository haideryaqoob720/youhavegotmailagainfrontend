import Vue from 'vue';
import Router from 'vue-router';
import LoginForm from './view/headerfroms/Login';
import SignupForm from './view/headerfroms/Signup';
Vue.use(Router);

export default new Router({
  history: true,
  routes: [
    {
      path: '/',
      component: () => import('./components/LandingPage.vue'),
      children: [
        {
          path: '/signup',
          name: 'Signup',
          component: SignupForm
        },
        {
          path: '',
          name: 'LoginForm',
          component: LoginForm
        }
      ]
    },
    {
      path: '/home',
      component: () => import('./components/Home.vue'),
      name: 'Home',
      children: [
        {
          path: '/conversation',
          name: 'ConversationStarter',
          component: () => import('./view/homenavigation/Conversation.vue')
        },
        {
          path: '/noMessage',
          name: 'noMessage',
          component: () => import('./view/homenavigation/noMessage.vue')
        },
        {
          path: '/profile',
          name: 'profile',
          component: () => import('./view/homenavigation/profile.vue')
        },
        {
          path: '/updateprofile',
          name: 'UpdateProfile',
          props: true,
          component: () => import('./view/homenavigation/UpdateProfile.vue')
        },
        {
          path: '/suspectprofile',
          name: 'SuspectProfile',
          props: true,
          component: () => import('./view/homenavigation/SuspectProfile.vue')
        },
        {
          path: '/chat',
          name: 'Chat',
          props: true,
          component: () => import('./view/homenavigation/Chat.vue')
        },
        {
          path: '/ChatMsg',
          name: 'ChatMsg',
          props: true,
          component: () => import('./view/homenavigation/ChatMsg.vue')
        },
        {
          path: '/BlockedProfiles',
          name: 'BlockedProfiles',
          props: true,
          component: () => import('./view/homenavigation/BlockedUsers.vue')
        },
        {
          path: '',
          name: 'Discover',
          component: () => import('./view/homenavigation/Discover.vue')
        }
      ]
    }
  ]
});
